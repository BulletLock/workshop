<?php
    include("php/session.php");
    
    $query = mysqli_query($con,"SELECT * FROM workshops");

    mysqli_close($con);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Workshop - Homepage</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!-- Navigation Bar starts -->
    
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Workshops</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <?php
                if(isset($login_session)){
                    echo "<li><a href=\"dashboard.php\">Dashboard</a></li>";
                    echo "<li><a class=\"btn btn-default btn-outline btn-circle\" href=\"php/logout.php\">Log Out</a></li>";
                } else {
                    echo "<li><a class=\"btn btn-default btn-outline btn-circle\" href=\"login.html\">Log In</a></li>";
            } ?>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
    
     <div class="container">
       <?php 
         if(isset($query)) {
             while($result = mysqli_fetch_assoc($query)) {
                echo "<div class=\"mainbox col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1 \">                    
                    <div id=\"workshop\" class=\"panel panel-info\" >
                            <div class=\"panel-heading\">
                                <div class=\"panel-title\">".$result['name']."</div>
                            </div>     

                            <div class=\"panel-body\" >
                               <p>
                                   <span>Venue: ".$result['venue']."</span>
                               </p>
                               <p>
                                   <span>Begin Date: ".$result['begin']."</span><br><span>End Date: ".$result['end']."</span>
                               </p>
                                <p>
                                    ".$result['description']."
                                </p>

                                <div id=\"apply-button\" class=\"col-sm-12 controls\">";
                                if(isset($login_session)){
                                    echo "<input id=\"btn-login\" name=\"submit\" type=\"button\" href=\"#\" onclick=\"apply(".$userID.",".$result['workshopID'].")\" class=\"btn btn-info btn-lg\" value=\"Apply\" >";
                                    } else {
                                        echo "<a id=\"btn-login\" name=\"submit\" type=\"button\" href=\"login.html\" class=\"btn btn-info btn-lg\" >Apply </a>";
                                    }
                                echo "</div>
                                
                                    <div id=\"alert".$result['workshopID']."\"></div>

                            </div>                     
                    </div>
                </div>";
             }
         }
        ?>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <script src="js/scripts.js"></script>
  </body>
</html>