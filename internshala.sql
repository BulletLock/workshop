-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 06:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `internshala`
--

-- --------------------------------------------------------

--
-- Table structure for table `appliedworkshop`
--

CREATE TABLE `appliedworkshop` (
  `appliedID` int(11) NOT NULL,
  `applied_At` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(11) DEFAULT NULL,
  `workshopID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appliedworkshop`
--

INSERT INTO `appliedworkshop` (`appliedID`, `applied_At`, `userID`, `workshopID`) VALUES
(1, '2017-08-10 16:22:59', 2, 6),
(2, '2017-08-10 16:23:10', 2, 10),
(3, '2017-08-10 16:23:25', 2, 4),
(4, '2017-08-10 16:23:41', 2, 11),
(5, '2017-08-10 16:23:41', 2, 13),
(6, '2017-08-10 20:52:19', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `name`, `username`, `email`, `password`, `created_at`) VALUES
(1, 'Dummy', 'dummy', 'dummy@dummy.com', 'dummy', '2017-08-09 22:09:05'),
(2, 'Arvind Rathee', 'arvindrathee', 'arvindrathee1996@gmail.com', 'arvindrathee', '2017-08-10 01:20:56');

-- --------------------------------------------------------

--
-- Table structure for table `workshops`
--

CREATE TABLE `workshops` (
  `workshopID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `begin` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workshops`
--

INSERT INTO `workshops` (`workshopID`, `name`, `venue`, `begin`, `end`, `description`, `created_at`) VALUES
(1, 'Workshop 1', 'Pune, India', 'Jan 10, 2018', 'Jan 15, 2018', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae erat sed libero rhoncus sodales at tincidunt urna. Vestibulum malesuada ante at urna gravida iaculis. Nulla eu elit nec magna fermentum venenatis quis vehicula tortor. Vivamus sollicitudin nunc at blandit iaculis. Integer congue lorem sed leo congue, a pellentesque quam dapibus. Cras consequat turpis lectus, sit amet lobortis odio sollicitudin eget. Integer rutrum elementum justo quis maximus. Phasellus consectetur, elit vitae scelerisque varius, ipsum massa interdum ipsum, eget pretium dolor erat a ligula. Proin sed sem molestie, tempus odio ut, feugiat dolor. Fusce pretium commodo consectetur. Nulla facilisi. ', '2017-08-10 14:44:03'),
(2, 'Workshop 2', 'New Delhi, India', 'Aug 15, 2017', 'Aug 25, 2017', 'In sit amet rhoncus diam. Pellentesque vel tristique massa, id hendrerit lacus. Sed scelerisque lectus eros, dignissim venenatis turpis euismod sed. Maecenas ornare sed diam id vehicula. Quisque interdum sapien dui, posuere aliquet quam feugiat id. Praesent dolor neque, elementum sed rutrum vel, tincidunt iaculis diam. Proin gravida, nisi a ullamcorper tincidunt, ante libero mollis lorem, ac viverra augue odio ac urna. Curabitur eu purus nec diam consequat luctus. Aliquam eu ligula vitae urna lacinia porttitor. Maecenas ac pellentesque nulla, sit amet viverra enim. ', '2017-08-10 14:46:16'),
(3, 'Workshop 3', 'Rohtak, India', 'Sep 10, 2017', 'Sep 15, 2017', 'Phasellus ut fringilla risus. Fusce id tortor ligula. Fusce sodales ex neque, eget tincidunt eros condimentum vel. Fusce id laoreet turpis. Duis vel felis et lectus porttitor dignissim vitae nec justo. Cras vestibulum non leo ac fringilla. Nulla ultrices, nisl id interdum dapibus, turpis lorem sagittis lorem, a tincidunt nulla felis sed tellus. Curabitur justo sapien, faucibus eu nisl quis, pretium sollicitudin purus. Curabitur vitae nulla in odio sagittis feugiat. ', '2017-08-10 14:48:41'),
(4, 'Workshop 4', 'Goa, India', 'Oct 11, 2017', 'Oct 15, 2017', 'Proin iaculis hendrerit mi, eget viverra tellus fringilla vitae. Aliquam ante sapien, posuere sed efficitur at, hendrerit a dui. Cras egestas ornare metus, vitae tincidunt arcu molestie eget. Cras pulvinar magna sed velit efficitur rhoncus. Pellentesque at sapien vel eros pellentesque commodo. Aenean rhoncus vehicula augue eu condimentum. Integer vel nisi nec ex vehicula vehicula vehicula ut tortor. Mauris blandit interdum vestibulum. Morbi faucibus bibendum tempus. Duis euismod porttitor ipsum. Sed eget accumsan est. ', '2017-08-10 15:05:36'),
(5, 'Workshop 5', 'Jaipur, India', 'Nov 15, 2017', 'Nov 25, 2017', 'Sed eget consectetur neque. Fusce elementum accumsan est, sed ornare tortor viverra vel. Donec in massa est. Vivamus commodo a lectus a porta. Pellentesque ac justo sapien. Proin gravida porta dignissim. Fusce id vehicula libero. Etiam eleifend fringilla ultricies. Suspendisse in ante vitae orci sagittis facilisis non nec sem. Vestibulum congue imperdiet cursus. Vestibulum eu rhoncus mi. Phasellus at molestie odio. Quisque rutrum sollicitudin justo, eu luctus erat rutrum sed. Donec posuere ullamcorper vehicula. Duis sit amet turpis turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2017-08-10 15:05:36'),
(6, 'Workshop 6', 'Hisar, India', 'Dec 20, 2017', 'Dec 25, 2017', 'Fusce rhoncus sit amet ante quis auctor. Quisque aliquam metus eget velit rutrum vulputate non quis ante. Integer sit amet ornare sapien. Proin posuere sed ante non sagittis. Mauris sed tortor nec felis malesuada sollicitudin at id sapien. Mauris at aliquam tellus, nec molestie nibh. Nulla pulvinar commodo aliquam. Suspendisse consequat ligula et congue aliquam. Phasellus volutpat cursus dolor feugiat semper. Sed eu sapien sed risus venenatis scelerisque eu eu magna. Maecenas sed dui in purus venenatis pellentesque. ', '2017-08-10 15:05:36'),
(7, 'Workshop 7', 'Mumbai, India', 'Jan 16, 2018', 'Jan 18, 2018', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt lacus. Etiam maximus auctor turpis et scelerisque. Quisque ante ante, condimentum eu fringilla quis, aliquet id sem. Donec feugiat diam quis venenatis efficitur. Maecenas sit amet lacinia urna. Etiam suscipit et mi a congue. Nam quis molestie urna. Praesent elementum elit ut tempus laoreet. Nulla elit sapien, cursus molestie malesuada sit amet, dictum pharetra nunc. Mauris quis accumsan nunc. ', '2017-08-10 15:05:36'),
(8, 'Workshop 8', 'Bhopal, India', 'Feb 26, 2018', 'Feb 28, 2018', 'Nulla tincidunt ex at elit suscipit pellentesque. Pellentesque ullamcorper ligula in neque pharetra, id condimentum magna placerat. Nam nisl magna, dignissim eu venenatis nec, luctus vel elit. Ut sed finibus ex, vitae aliquet mi. Integer mollis placerat malesuada. Maecenas vestibulum felis in ultricies consequat. Donec sagittis congue orci, nec laoreet ante sodales nec. Sed ut volutpat nisl. Sed varius a sapien in venenatis. Pellentesque eu augue neque. Nunc sit amet metus tortor. ', '2017-08-10 15:05:36'),
(9, 'Workshop 9', 'New Delhi, India', 'Sep 13, 2017', 'Sep 16, 2017', 'Fusce a pretium ante. Phasellus tempus diam lacus, in tincidunt augue elementum mattis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed nec commodo dolor. Fusce id gravida neque. Morbi elementum finibus arcu sit amet porta. Curabitur eu efficitur metus. Vivamus vitae maximus erat, quis blandit justo. Donec vitae est sit amet turpis convallis fermentum. Quisque porttitor feugiat sapien eget fermentum. Praesent sit amet diam semper, porttitor mauris ornare, volutpat dolor. Morbi auctor ornare felis at condimentum. ', '2017-08-10 15:05:36'),
(10, 'Workshop 10', 'Rohtak, India', 'Mar 9, 2018', 'Mar 15, 2017', 'Curabitur commodo id neque et viverra. Maecenas dictum vel enim eu elementum. Duis ultricies pellentesque magna nec tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec at consequat nunc. Aliquam et libero sed libero dignissim pulvinar. Donec non suscipit tellus, et cursus elit. Quisque nisi ipsum, ultrices eget est quis, iaculis lobortis magna. Morbi at velit pharetra nulla gravida consequat et at ante. Integer pretium diam mi, id interdum lacus vestibulum nec. Phasellus non gravida augue, fermentum finibus nunc. Aenean urna tellus, sodales ac dui et, posuere lacinia magna.', '2017-08-10 15:05:36'),
(11, 'Workshop 11', 'Jaipur, India', 'Aug 4, 2017', 'Aug 10, 2017', 'Sed vitae nunc ac ante facilisis tempus ac efficitur ex. Duis sed eros sit amet mi faucibus mollis. Curabitur magna augue, hendrerit auctor finibus in, convallis nec dui. Nulla tempus orci nec dolor malesuada eleifend. Nulla sollicitudin massa nec justo convallis, vitae fermentum justo condimentum. Quisque a erat consequat, placerat leo eget, varius dolor. Nullam risus ipsum, rhoncus sed finibus ultricies, dapibus eget nibh. Suspendisse tristique enim eu massa eleifend, tempus ultrices turpis faucibus. ', '2017-08-10 15:05:36'),
(12, 'Workshop 12', 'Mumbai, India', 'Oct 18, 2017', 'Oct 23, 2017', 'Phasellus ut fringilla risus. Fusce id tortor ligula. Fusce sodales ex neque, eget tincidunt eros condimentum vel. Fusce id laoreet turpis. Duis vel felis et lectus porttitor dignissim vitae nec justo. Cras vestibulum non leo ac fringilla. Nulla ultrices, nisl id interdum dapibus, turpis lorem sagittis lorem, a tincidunt nulla felis sed tellus. Curabitur justo sapien, faucibus eu nisl quis, pretium sollicitudin purus. Curabitur vitae nulla in odio sagittis feugiat. ', '2017-08-10 15:05:36'),
(13, 'Workshop 13', 'Pune, India', 'Nov 19, 2017', 'Nov 23, 2017', 'Fusce a pretium ante. Phasellus tempus diam lacus, in tincidunt augue elementum mattis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed nec commodo dolor. Fusce id gravida neque. Morbi elementum finibus arcu sit amet porta. Curabitur eu efficitur metus. Vivamus vitae maximus erat, quis blandit justo. Donec vitae est sit amet turpis convallis fermentum. Quisque porttitor feugiat sapien eget fermentum. Praesent sit amet diam semper, porttitor mauris ornare, volutpat dolor. Morbi auctor ornare felis at condimentum. ', '2017-08-10 15:05:36'),
(14, 'Workshop 14', 'Imphal, India', 'Dec 28, 2017', 'Dec 30, 2017', 'Nulla tincidunt ex at elit suscipit pellentesque. Pellentesque ullamcorper ligula in neque pharetra, id condimentum magna placerat. Nam nisl magna, dignissim eu venenatis nec, luctus vel elit. Ut sed finibus ex, vitae aliquet mi. Integer mollis placerat malesuada. Maecenas vestibulum felis in ultricies consequat. Donec sagittis congue orci, nec laoreet ante sodales nec. Sed ut volutpat nisl. Sed varius a sapien in venenatis. Pellentesque eu augue neque. Nunc sit amet metus tortor. ', '2017-08-10 15:05:36'),
(15, 'Workshop 15', 'Amritsar, India', 'Sep 11, 2017', 'Sep 15, 2017', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt lacus. Etiam maximus auctor turpis et scelerisque. Quisque ante ante, condimentum eu fringilla quis, aliquet id sem. Donec feugiat diam quis venenatis efficitur. Maecenas sit amet lacinia urna. Etiam suscipit et mi a congue. Nam quis molestie urna. Praesent elementum elit ut tempus laoreet. Nulla elit sapien, cursus molestie malesuada sit amet, dictum pharetra nunc. Mauris quis accumsan nunc. ', '2017-08-10 15:05:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appliedworkshop`
--
ALTER TABLE `appliedworkshop`
  ADD PRIMARY KEY (`appliedID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `workshopID` (`workshopID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `workshops`
--
ALTER TABLE `workshops`
  ADD PRIMARY KEY (`workshopID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appliedworkshop`
--
ALTER TABLE `appliedworkshop`
  MODIFY `appliedID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `workshops`
--
ALTER TABLE `workshops`
  MODIFY `workshopID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `appliedworkshop`
--
ALTER TABLE `appliedworkshop`
  ADD CONSTRAINT `appliedworkshop_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `appliedworkshop_ibfk_2` FOREIGN KEY (`workshopID`) REFERENCES `workshops` (`workshopID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
