
<?php
    include('php/session.php');

    if(!isset($login_session)){
        mysqli_close($con); // Closing Connection
        header('Location: login.html'); // Redirecting To Home Page
    }

    $query = mysqli_query($con,"SELECT workshops.name as wname, appliedworkshop.applied_At as wdate FROM workshops, appliedworkshop WHERE workshops.workshopID = appliedworkshop.workshopID && appliedworkshop.userID= '$userID' ");
    
    mysqli_close($con);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dashboard</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
       
    <!-- Navigation Bar starts -->
    
     <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Workshops</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="dashboard.php">Dashboard</a></li>
            <li>
              <a class="btn btn-default btn-outline btn-circle" href="php/logout.php">Log Out</a>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
     <div class="container">
        
            <div id="head">
                <h1>Welcome! You have applied for these workshops</h1>
            </div>    

            <table class="table table-striped table-responsive">
                    <tr>
                        <th>S .No. </th>
                        <th>Name</th>
                        <th>Applied Date</th>
                    </tr>
                    <?php
                         if(isset($query)) {
                             $x = 1;
                             while($result = mysqli_fetch_assoc($query)) {
                                 echo "<tr>
                                    <td>".$x."</td>
                                    <td>".$result['wname']."</td>
                                    <td>".$result['wdate']."</td>
                                  </tr>";
                                 $x += 1;
                             }
                         }
                    ?>
            </table>
        
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <script src="js/scripts.js"></script>
  </body>
</html>