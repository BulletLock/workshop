How to run this program?

    1. Install and run XAMPP on your computer
    2. Unzip the file into xampp/htdocs/internshala directory
    3. Visit http://localhost/phpmyadmin
    4. Now create a database named internshala
    5. Import internshala.sql into your database
    6. Visit http://localhost/internshala
    7. To navigate freely two users have been already created
        - username = arvindrathee , pass = arvindrathee
        - username = dummy , pass = dummy
    8. If you want to create or change anything related to database credentials visit php/config.php
    9. Edit it as per your requirements.

=======================================================
        LIVE DEMO ==>  http://workshop.iarvind.xyz 
=======================================================