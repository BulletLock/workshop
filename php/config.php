<?php
    // Define variable containing database credentials
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "internshala";

    // connect to MySQL
    $con = mysqli_connect($dbhost, $dbuser, $dbpass);
    
    // If connection is not made
    if (!$con) {
        die('Could not connect: ' . mysql_error());
    }

    // Connect to the database
    mysqli_select_db($con, $dbname);

?>