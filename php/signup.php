<?php
   
    include("config.php");

    //===================================================
    // Process data sent via client                    //
    //Escape User Input to help prevent SQL Injection  //
    //===================================================

    $fname = mysqli_real_escape_string($con,$_POST['fname']);
    $lname = mysqli_real_escape_string($con,$_POST['lname']);
    $name = $fname . " " . $lname;
    $uname = mysqli_real_escape_string($con,$_POST['uname']);
    $upass = mysqli_real_escape_string($con,$_POST['upass']);
    $uemail = mysqli_real_escape_string($con,$_POST['uemail']);

    //=============================================
    // Query database to check if username or    //
    // email is already present in the database  // 
    // otherwise create a new user.              //
    //=============================================

    $query = mysqli_query($con,"SELECT username,email FROM users WHERE username= '$uname' or email= '$uemail' ");
    if (mysqli_num_rows($query) != 0) {
        echo "Username or email already exists";
    } else {
        $query_create =  "INSERT INTO users (name, username, email, password )
                          VALUES ('$name', '$uname', '$uemail', '$upass')";
        if (!mysqli_query($con,$query_create)) {
          die('Error: ' . mysqli_error($con));
        echo "Account has been created please visit login page";
        // echo "DONE" check for this response in ajax callback
    }
        
    }

    // close connection to the database
    mysqli_close($con);
    
?>