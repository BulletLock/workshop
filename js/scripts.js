//===================================================
//                   User Registration             //
//===================================================

function signupFunction(){
    var ajaxRequest;  // The variable that makes Ajax possible!
    
    try {
        // Opera 8.0+, Firefox, Safari
        ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            // Internet Explorer Browsers
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    // Something went wrong
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    
    //=============================================
    // Create a function that will receive data  //
    // sent from the server and will update      //
    // div section in the same page.             //
    //=============================================
					
    ajaxRequest.onreadystatechange = function(){
        if(ajaxRequest.readyState == 4 && ajaxRequest.status == 200){    //check for data=='DONE' here if its done change the dom
            var ajaxDisplay = document.getElementById('ajaxDiv');
            ajaxDisplay.innerHTML = ajaxRequest.responseText;
        }
    };
    
    //================================================
    // Now get the value from user and pass it to   //
    // server script.                               //
    //================================================
    
    
    error = ''; // empty string which will store error messages if any
    
    //================================================
    //              Firstname validation            //
    //================================================
    
    var firstname = document.getElementById('firstname').value;
    
    var letters = /^[A-Za-z]+$/;  // Regex
    // Checks if name is valid if not error is displayed
    if(firstname.match(letters) && firstname.length >=0) {  
        var fname = firstname;  
    } else {  
        error += "Firstname must have alphabet characters only <br>";
    }
    
    //================================================
    //              Lastname validation             //
    //================================================
    
    var lastname = document.getElementById('lastname').value;
    
    // Checks if name is valid if not error is displayed
    if(lastname.match(letters) && lastname.length >=0) {  
        var lname = lastname;  
    } else {  
        error += "Lastname must have alphabet characters only <br>"; 
    }
    
    //================================================
    //              Username validation             //
    //================================================
    
    var username = document.getElementById('username').value;
    
    var username_len = username.length; 
    // Checks if username is valid if not error is displayed
    if (username_len === 0 || username_len <= 8 || username_len > 32) {  
        error += "Username should not be empty / length be between "+8+" to "+32 + "<br>"; 
    } else {  
        var uname = username; 
    }
    
    //===============================================
    //              Email validation               //
    //===============================================
    
    var email = document.getElementById('email').value;
    
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  //regex
    // Checks if email is valid if not error is displayed
    if(email.match(mailformat) && email.length >= 0) {  
        var uemail = email;  
    } else {  
        error += "You have entered an invalid email address! <br> ";
    }
    
    //===============================================
    //              Password validation            //
    //===============================================
    
    var password = document.getElementById('password').value;
    
    // Checks if password is valid if not error is displayed
    var password_len = password.length;  
    if (password_len === 0 ||password_len <= 8 || password_len > 32) {  
        error += "Password should not be empty / length be between "+8+" to "+32 + "<br>";   
        
    }else {
        var upass = password;
    }
    
    // ==============================
    // If error string is empty    //
    // nothing is shown on screen  //
    // else error are displayed    //
    //===============================
    
    if (error.length !== 0) { 
        document.getElementById('signupalert').innerHTML = error;
        document.getElementById('signupalert').style.display = "block";
    } else {
        document.getElementById('signupalert').style.display = "none";
    }
    
    if (error.length ===0) {
        // Concatenating data that is to be sent to server
        var dataString = "fname="+fname+"&lname="+lname+"&uname="+uname+"&uemail="+uemail+"&upass="+upass;

        //===========================
        // Using AJAX data is sent //
        //===========================

        ajaxRequest.open("POST", "php/signup.php", true);
        ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajaxRequest.send(dataString); 
    }
} // End Signup Function


//===========================================
//                User Login               //
//===========================================

function loginFunction() {
    var ajaxRequest;  // The variable that makes Ajax possible!
    
    try {
        // Opera 8.0+, Firefox, Safari
        ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            // Internet Explorer Browsers
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    // Something went wrong
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    
    //=============================================
    // Create a function that will receive data  //
    // sent from the server and will update      //
    // div section in the same page.             //
    //=============================================
					
    ajaxRequest.onreadystatechange = function(){
        if(ajaxRequest.readyState == 4 && ajaxRequest.status == 200){
            if ( ajaxRequest.responseText == 'success' ){
                window.location.href = 'dashboard.php';
            } else {
            var ajaxDisplay = document.getElementById('login-alert');
            ajaxDisplay.innerHTML = ajaxRequest.responseText;
            console.log(ajaxRequest.responseText);
            ajaxDisplay.style.display = "block";
            //if () {
            //    window.location.href = 
            }
        }
    };
    
    
    //================================================
    // Now get the value from user and pass it to   //
    // server script.                               //
    //================================================
    
    
    var username = document.getElementById('login-username').value;
    
    var password = document.getElementById('login-password').value;
    
    // Concatenating data that is to be sent to server
    dataString = "uname="+username+"&upass="+password;
    
    
    
    //===========================
    // Using AJAX data is sent //
    //===========================
    
    ajaxRequest.open("POST", "php/login.php", true);
    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajaxRequest.send(dataString); 
    
} // Login function End

function apply(userid,workshopid) {
    var ajaxRequest;  // The variable that makes Ajax possible!
    
    try {
        // Opera 8.0+, Firefox, Safari
        ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            // Internet Explorer Browsers
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    // Something went wrong
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    
    
    //=============================================
    // Create a function that will receive data  //
    // sent from the server and will update      //
    // div section in the same page.             //
    //=============================================
					
    ajaxRequest.onreadystatechange = function(){
        if(ajaxRequest.readyState == 4 && ajaxRequest.status == 200){
            if ( ajaxRequest.responseText == 'success' ){
                window.location.href = 'dashboard.php';
            } else {
                var ajaxDisplay = document.getElementById('alert'+workshopid);
                ajaxDisplay.innerHTML = ajaxRequest.responseText;
                console.log(ajaxRequest.responseText);
            //if () {
            //    window.location.href = 
            }
        }
    };
    
    
    //================================================
    // Now get the value from user and pass it to   //
    // server script.                               //
    //================================================
    
    // Concatenating data that is to be sent to server
    dataString = "userid="+userid+"&workshopid="+workshopid;
    
    
    
    //===========================
    // Using AJAX data is sent //
    //===========================
    
    ajaxRequest.open("POST", "php/apply.php", true);
    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajaxRequest.send(dataString); 
    
    
}